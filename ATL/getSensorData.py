#!/usr/bin/env python3

import RPi.GPIO as gpio
import dht11
import spidev
import time
import os

# LED (indicates if the plant needs water)
led = 21
gpio.setmode(gpio.BCM)
gpio.setup(led, gpio.OUT)

# DHT11 (air tempreature and humidity sensor)
sensor = dht11.DHT11(pin=4)

# open SPI (needed for the analog/digital converter MCP3008
spi = spidev.SpiDev()
spi.open(0,0)
spi.max_speed_hz=1000000

# get data from MCP3008
def analogInput(channel):
  adc = spi.xfer2([1,(8+channel)<<4,0])
  data = ((adc[1]&3) << 8) + adc[2]
  return data

def pflanze_messen(): 

  helligkeit = analogInput(0)
  # MCP300 gives you a numeric value from 0 - 1023, where 1023 is the darkest. this calcs it into a percentage value
  helligkeit = round(100 - (int(helligkeit) / 1023 * 100),0)

  bodenFeuchtigkeitNum = analogInput(1)

  if bodenFeuchtigkeitNum > 650: # example value for a plant that needs some more water. 
    gpio.output(led, gpio.HIGH) # turns on LED to indicate that the plant needs water
  else:
    gpio.output(led, gpio.LOW)

  # MCP300 gives you a numeric value from 0 - 1023, where 1023 is the driest. this calcs it into a percentage value
  bodenFeuchtigkeit = round(100 - (bodenFeuchtigkeitNum / 1023 * 100), 1)

  # repeat until result from dht11 is valid
  while True:
    result = sensor.read()
    if result.is_valid():
      # Logs values to stdout and returns them
      print("===========================")
      print("Bodenfeuchtigkeit: "+str(bodenFeuchtigkeit)+"%, Numerisch: "+str(bodenFeuchtigkeitNum))
      print("Helligkeit: "+str(helligkeit))
      print("Temperatur: "+str(result.temperature))
      print("Luftfeuchtigkeit: "+str(result.humidity)+"%")

      return str(bodenFeuchtigkeit), str(helligkeit), str(result.temperature), str(result.humidity)
      break
