from flask import Flask, render_template, request
import sqlite3

app = Flask(__name__)

# get data from DB
def getData():
  conn=sqlite3.connect('sensorData.db')
  curs=conn.cursor()
  for row in curs.execute("SELECT * FROM sensorData ORDER BY timestamp DESC LIMIT 1"):
    time = str(row[0])
    gHum = row[1] # Ground Humidity
    bright = row[2] # brightness
    temp = row[3] # temperature
    aHum = row[4] # Air Humidity
    conn.close()
    return time, gHum, bright, temp, aHum

# runs python webserver and uses flask to generate the webpage
@app.route("/")
def index():	
  time, gHum, bright, temp, aHum = getData()
  templateData = {
    'time': time,
    'gHum': gHum,
    'bright': bright,
    'temp': temp,
    'aHum': aHum
  }
  return render_template('index.html', **templateData)

if __name__ == '__main__':
   app.run(host='0.0.0.0', port=80, debug=False)
