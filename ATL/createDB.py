#!/usr/bin/env python3

import sqlite3 as lite
import sys

con = lite.connect('sensorData.db') 
with con: 
  cur = con.cursor() 
  cur.execute("DROP TABLE IF EXISTS sensorData")
  cur.execute("""
    CREATE TABLE sensorData(
      timestamp DATETIME, 
      bFeuchtigkeit NUMERIC, 
      helligkeit NUMERIC,
      temp NUMERIC, 
      lFeuchtigkeit NUMERIC)""")
