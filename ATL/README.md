## Pre-requirements
All hardware from https://www.pi-shop.ch/raspberry-pi-das-schlaue-pflanze-kit + Raspberry Pi 3 Model B

### Software
OS: Raspberry Pi OS Lite (Debian 10)

Packages:

`apt install -y build-essential python3 python3-dev python3-pip git sqlite3`

`pip3 install dht11 sqlite3`

## Run project
`git clone https://gitlab.com/an00bis/IOT.git`

`chmod +x startServer.sh stopServer.sh`

Start the server: `./startServer.sh`

Stop the server: `./stopServer.sh`
