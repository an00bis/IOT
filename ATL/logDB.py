#!/usr/bin/env python3

import time
import sqlite3
import getSensorData

dbname='sensorData.db'
timeOut = 30 # time in seconds

# log sensor data on database
def logData (bFeuchtigkeit, helligkeit, temperatur, lFeuchtigkeit):
	
	conn=sqlite3.connect(dbname)
	curs=conn.cursor()
	
	curs.execute("INSERT INTO sensorData values(datetime('now', 'localtime'), (?), (?), (?), (?))", (bFeuchtigkeit, helligkeit, temperatur, lFeuchtigkeit))
	conn.commit()
	conn.close()

while True:
  bFeuchtigkeit, helligkeit, temperatur, lFeuchtigkeit = getSensorData.pflanze_messen()
  logData(bFeuchtigkeit, helligkeit, temperatur, lFeuchtigkeit)
  time.sleep(timeOut)
