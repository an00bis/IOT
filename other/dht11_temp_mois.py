#!/usr/bin/env python3

import RPi.GPIO as GPIO
import dht11

sensor = dht11.DHT11

pin = 4
feuchtigkeit, temperatur = dht11.read_retry(sensor, pin)

print("Temperatur: "+str(temperatur) + "°C, Feuchtigkeit: "+str(feuchtigkeit)+"%")
