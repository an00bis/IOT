#!/usr/bin/env python3

import spidev
import time
import os
import RPi.GPIO as gpio


spi = spidev.SpiDev()
spi.open(0,0)

# MCP3008
def analogEingang(channel):
  adc = spi.xfer2([1,(8+channel)<<4,0])
  data = ((adc[1]&3) << 8) + adc[2]
  return data

while True:
  print("Feuchtigkeit: "+str(analogEingang(1)))
  time.sleep(0.2)
